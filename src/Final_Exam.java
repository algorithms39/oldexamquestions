import java.util.ArrayList;
import java.util.Iterator;

public class Final_Exam {
    public static void main(String[] args) {
        int[] prices1 = new int[] { 1, 5, 8, 9, 10, 17, 17, 20 };
        int[] prices2 = new int[] { 3, 5, 8, 9, 10, 17, 17, 20 };
        System.out.println("Max total price 1 : " + findMaxDynamic(8, prices1));
        System.out.println("Max total price 2 : " + findMaxDynamic(8, prices2));

        ArrayList<Job> jobsAndDeadlines = new ArrayList<>();
        jobsAndDeadlines.add(new Job('a', 100, 2));
        jobsAndDeadlines.add(new Job('b', 19, 1));
        jobsAndDeadlines.add(new Job('c', 27, 2));
        jobsAndDeadlines.add(new Job('d', 25, 1));
        jobsAndDeadlines.add(new Job('e', 15, 3));

        JobScheduler jobScheduler = new JobScheduler();

        int maxProfit = jobScheduler.scheduleJobsRecursiveGreedy(jobsAndDeadlines);

        System.out.println("CPU Scheduling with max profit: " + maxProfit);
    }

    public static int findMaxRecursive(int n, int[] prices) {
        if (n == 0) {
            return 0;
        }
        int maxTotalPrice = 0;
        for (int i = 0; i < n; i++) {
            int totalPrice = findMaxRecursive(i, prices) + prices[n - i - 1];
            maxTotalPrice = Math.max(maxTotalPrice, totalPrice);
        }

        return maxTotalPrice;
    }

    public static int findMaxDynamic(int n, int[] prices) {
        int[] table = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j < prices.length; j++) {
                if (i + j + 1 <= n) {
                    table[i + j + 1] = Math.max(table[i + j + 1], table[i] + prices[j]);
                }
            }
        }
        return table[n];
    }

}

class JobScheduler {
    public int scheduleJobsRecursiveGreedy(ArrayList<Job> jobsAndDeadlines) {
        return scheduleJobsRecursiveGreedy(jobsAndDeadlines, 0);
    }

    private int scheduleJobsRecursiveGreedy(ArrayList<Job> jobsAndDeadlines, int maxProfit) {
        if (jobsAndDeadlines.isEmpty()) {
            return maxProfit;
        }

        // Initialy set it to first job in the array.
        Job currentMaxProfitJob = jobsAndDeadlines.get(0);

        // Had to use iterator because you can not remove an item from the Arraylist in
        // a for loop: ConcurrentModificationException
        Iterator<Job> iterator = jobsAndDeadlines.iterator();
        while (iterator.hasNext()) {
            Job job = iterator.next();

            // If another job's profit is higher than current, make it currentMaxProfitJob.
            if (job.profit > currentMaxProfitJob.profit) {
                currentMaxProfitJob = job;
            }

            // A unit of time has been passed.
            // Reduce every jobs' deadlines by -1.
            job.deadline--;

            if (job.deadline == 0) {
                // Remove the job if deadline is 0.
                iterator.remove();
            }
        }

        try {
            // ! Remove the currentMaxProfitJob since CPU selected it and executed it.
            jobsAndDeadlines.remove(currentMaxProfitJob);
        } catch (Exception ex) {
            // Already removed due to deadline.
        }

        return scheduleJobsRecursiveGreedy(jobsAndDeadlines, maxProfit + currentMaxProfitJob.profit);
    }
}

class Job {
    char id;
    int profit;
    int deadline;

    public Job(char id, int profit, int deadline) {
        this.id = id;
        this.profit = profit;
        this.deadline = deadline;
    }

}