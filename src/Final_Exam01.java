public class Final_Exam01 {
    public static void main(String[] args) {
        int n = 7;
        int[] weights = new int[] { 1, 2, 3, 5 };
        System.out.println(minItemsDynamic(n, weights));
    }

    public static int minItemsRecursive(int n, int[] weights) {
        if (n == 0) {
            return 0;
        }
        int numOfWeights = Integer.MAX_VALUE;
        for (int i = 0; i < weights.length; i++) {
            if (n - weights[i] >= 0) {
                int numItems = minItemsRecursive(n - weights[i], weights) + 1;
                numOfWeights = Math.min(numItems, numOfWeights);
            }
        }
        return numOfWeights;
    }

    public static int minItemsDynamic(int n, int[] weights) {
        int[] table = new int[n + 1];

        for (int i = 0; i <= n; i++) {
            table[i] = Integer.MAX_VALUE;
        }

        table[0] = 0;

        for (int i = 0; i <= n; i++) {
            for (int j = 0; j < weights.length; j++) {
                int range = i + weights[j];
                if (range <= n) {
                    table[range] = Math.min(table[i] + 1, table[range]);

                }
            }
        }
        return table[n];
    }

    public static int minItemsDynamicAnotherSolution(int bagCapacity) {
        // Table to store the results of subproblems
        int[] table = new int[bagCapacity + 1];

        // Seed the trivial answer into the table.
        table[0] = 0; // If bag capacity is 0, 0 items
        table[1] = 1; // If bag capacity is 1, 1 x 1kg
        table[2] = 2; // If bag capacity is 2, 2 x 1kg
        table[3] = 1; // If bag capacity is 3, 1 x 3kg
        table[4] = 2; // If bag capacity is 4, 2 x 2kg
        table[5] = 1; // If bag capacity is 5, 1 x 5kg

        // Iterate through the table and
        // fill further positions based on the current positions
        for (int i = 0; i < bagCapacity; i++) {
            try {
                table[i + 5] = table[i] + 1;
            } catch (ArrayIndexOutOfBoundsException ex) {
                // Exceed the border of array. Do nothing.
            }
        }

        return table[bagCapacity];
    }
}
