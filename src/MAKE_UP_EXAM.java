public class MAKE_UP_EXAM {
    public static void main(String[] args) {
        int[] price = {3, 2, 5, 9};
        int n = 4;

        int minTotalPrice = minTotalPriceRecursive(price, n);

        System.out.println("Minimum total price: " + minTotalPrice);


    }


    public static int travellerDynamic(int[] price, int n) {
        int[] dp = new int[n + 1];
        for (int i = 0; i < price.length; i++) {
            dp[i + 1] = price[i];
        }

        for (int i = 2; i <= n; i++) {
            for (int j = 0; j < price.length; j++) {
                int range = i + j;
                if (range <= n) {
                    dp[i + j] = Math.min(price[j] + dp[i-1], dp[i + j]);
                }

            }
        }

        return dp[n];
    }
    public static int minTotalPriceRecursive(int[] price, int n) {
        if (n == 0) {
            return 0;
        }

        int minTotalPrice = Integer.MAX_VALUE;
        for (int j = 0; j < n; j++) {
            int totalPrice = minTotalPriceRecursive(price, j) + price[n-j-1];
            minTotalPrice = Math.min(minTotalPrice, totalPrice);
        }

        return minTotalPrice;
    }
    
}