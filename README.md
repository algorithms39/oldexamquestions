<details open><summary><b>Table of Content</b></summary><blockquote>

1. [Make-up Exam](#make-up-exam)

<details ><summary><b>Final Exam</b></summary><blockquote>

1. [Q1](#q1)
1. [Q2](#q2)
1. [Q3](#q3)
1. [Q4](#q4)

</blockquote>
</details>
<details ><summary><b>Final Exam1</b></summary><blockquote>

1. [Q1](#q1-1)
1. [Q2](#q2-1)
1. [Q3](#q3-1)

</blockquote>
</details>

</blockquote>

</details>

## MAKE-UP EXAM

**Question 1.** A traveler wants to walk a road of a given distance _n_. There is a cafe on his/her road
placed in **every** discrete location _i Є {1, 2, ..., n}_. The traveler has to rest and have a coffee at some of
these cafe locations all of them having have changing coffee prices depending on **the distance he has
walked since the last location he rested**.

The prices for all possible distances are stored in a _price_ array. That is, _price[1]_ denotes the price that
the traveler has to pay for the coffee if s/he walked a distance of _1_ since the last location s/he rested _,
price[2]_ denotes the price that he has to pay if s/he walked a distance of _2_ since the last location s/he
rested and so on. For example; when _n = 9_ , if s/he firstly rests at location _3_ , s/he has to pay _price[3]_
and then, if s/he rests at location _5_ , s/he has to pay _price[2]_ and then, if s/he rests at location _9_ , s/he
has to pay _price[4]_. Hence, the total price makes “ _price[3] + price[2] + price[4]”_.

Your goal is to minimize the total price s/he has to pay.

For example; when _n = 4_ and _price = [3,2,5,9]_ , the optimum locations that s/he has to rest are _2_ and _4_
as shown in the table below.

| Stop Locations | Total Price |
| :------------: | :---------: |
|       4        |      9      |
|      3,4       |    5+3=8    |
|      2,4       |    2+2=4    |
|      1,4       |    3+5=8    |
|     2,3,4      |   2+3+3=8   |
|     1,3,4      |   3+2+3=8   |
|     1,2,4      |   3+3+2=8   |
|    1,2,3,4     | 3+3+3+3=12  |

Given a distance _n_ and a _price_ array,

1. (20pts.) Give a mathematical recursive formulation for P(n) where P(n) denotes the minimum
   price the traveler has to pay when he wants to walk a distance of n.
    - ``` P(n) = min(P(j) + price[n-j]) for all 0 < j < n ```
1. (20pts.) Write a recursive algorithm (i.e., a pseudocode) that returns the minimum price the
   traveler has to pay when he wants to walk a distance of n.
    - [click to see solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/2e9f16056dffdf314a94179110e658a53a9f68a5/src/MAKE_UP_EXAM.java#L30-L42)


1. (20pts.) Give an example distance n and a price array to show that the greedy choice of every
   time choosing to walk the distance, which does not exceed the total distance n and would
   cost the minimum price at the next stop location, does not always lead to an optimum
   solution.
    - ```for price [8,6,7,9] greedy algorithms picks the min priced restaurant which is the second restaurant so the traveller will stop at the second restaurant and than again it will choose the second one. total price will be 6+6 = 12, but if it chose to stop dırectly at the 4th restaurant the total cost would be 9 ıt ıs smaller than the greedy. ```
1. (30pts.) Write a dynamic programming algorithm (i.e., a pseudocode) for finding the
   minimum price the traveler has to pay to walk a distance of n.
    - [click to see solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/2e9f16056dffdf314a94179110e658a53a9f68a5/src/MAKE_UP_EXAM.java#L12-L29)

1. (10 pts.) Provide the running time of your dynamic programming algorithm. Explain.
    - ``` The outer loop iterates through the locations from 2 to n, and the inner loop iterates through all elements of the price array. This means that the total number of iterations is n * n, which is equivalent to O(n^2).  ```

**P.S.** It is extremely necessary to understand the question (e.g. what the _price_ array holds).

# Final Exam

## Q1

Q-1) (50 points) Given a rod of length n inches and an array A[1...n] of prices
that includes prices of all pieces of discrete sizes smaller than or equal to n.
Determine the maximum value obtainable by cutting up the rod and selling the
pieces. For example, if the length of rod is 8 and the values of different pieces
are given as the following, then the maximum obtainable value is 22 (by
cutting in two pieces of lengths 2 and 6)

length | 1 2 3 4 5 6 7 8
------------------------------------------------------------------------------------
price | 1 5 8 9 10 17 17 20

And if the prices are as following, then the maximum obtainable value is 24 (by
cutting in eight pieces of length 1)

length | 1 2 3 4 5 6 7 8
--------------------------------------------------------------------------------------
price | 3 5 8 9 10 17 17 20

a) (25 pts.) Give a mathematical recursive formulation for R(n) which denotes
the maximum possible price value you can get for a rod of length n.

- ``` R(n) = max(R(i) + prices[n-i]) for 0 < i < n ```
- [recursive solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/main/src/Final_Exam.java#L9-L20)

b) (5 pts.) Show that this problem has the overlapping subproblems property.

- ![image](./finalQ1-b.jpg)

c) (15 pts.) Write a dynamic programming algorithm (as a pseudocode) for
finding the maximum price that can be obtained for a rod of length n.

- [click to see solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/main/src/Final_Exam.java#L22-L32)

d) (5 pts.) Provide the running time of your dynamic programming algorithm.
Explain briefly.

- ``` it contaions nested two loop outer iterates from 0 to n, inner iterates n * prices.lentgth(basicly n). time complexity of the algorithm is O(n^2)  ```

## Q2

Q-2) (20 pts.) You are given an array of jobs where every job takes single (i.e.1)
unit of time. Also, every job has a deadline (i.e. latest time or date by which
something should be completed) and an associated profit if the job is finished
before the deadline. Thus, the minimum possible deadline for any job is 1. If
only one job can be scheduled at a time, the job sequencing problem is to
maximize total profit. Below is an example optimal solution for the given job
list:

Input: Five jobs with the following deadlines and profits  
**Output:** The maximum profit sequence of jobs is a, c, e.

| jobID | Deadline | Profit |
| :---: | -------- | ------ |
|   a   | 2        | 100    |
|   b   | 1        | 19     |
|   c   | 2        | 27     |
|   d   | 1        | 25     |
|   e   | 3        | 15     |

a) (10 pts.) Describe, in words, a greedy algorithm for the job sequencing
problem.

- ``` greedy algorithm will look for the highest profit job with the valid ones not the expired ones. it will pick the highest at first than will look again the highest and not expired one  ```

b) (10 pts.) Prove that your greedy choice is optimal (e.g. proof by
contradiction).

- ``` Lets suppose there is a non greedy algorithm that pick the first job call it j. when we compare with the profit by per unit there is others jobs which gives the highest profit. this contradicts with the nongreedy algorithm so, out greedy solution is optimal  ```

c) The greedy code for the problem: ``scheduleJobsRecursiveGreedy()``
- [click to see solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/main/src/Final_Exam.java#L52-L108)


## Q3

Q-3) (20 pts.) Assume you are creating an array data structure that has a fixed
size of n. You want to backup this array after every n insertion operations.
Unfortunately, the backup operation is quite expensive, it takes n time to do
backup. Insertions without a backup just take 1 time unit. Show that you can do
backups in O(1) amortized time. Use potential method.

## Q4

Q-4) (10 pts.) Given an unsorted array, the array has this property that every
element in array is at most k distance from its position in sorted array where k
is a variable smaller than size of array. For example, let us consider k is 2, an
element at index 7 in the sorted array, can be at indexes 5, 6, 7, 8, 9 in the
given array. We can sort such arrays more efficiently with the help of Heap data
structure. Following is the sorting algorithm that uses a Min-Heap:

- Create a Min-Heap of size k+1 with first k+1 elements.
- One by one remove min elements from heap, put it in result array, and
  add a new element to heap from remaining elements.

Provide a tight asymptotic upper bound for this algorithm. Explain briefly.

- ``` we remove element from the heap which takes log(k) time, adding to the heap also takes log(k) time. we are doing this process for the all n elements so the time complexity of this algorithm is O(nlog(k));  ```

# Final1 Exam

## Q1

**1.** (60 pts.) Imagine a thief entering a house. In the house, there are infinitely many items
that can have only one of three different weights: _1_ kg, _3_ kgs, and _5_ kgs. All of the items are
discrete. The thief has a bag capacity of _n_ kgs and strangely, he wants to steal the “smallest
number of items”.

1. (10 pts.) Give a mathematical recursive formulation for C(n) where C(n) denotes the
   smallest number of items the thief can steal using a bag capacity of n.
    - ``` C(n) = min(C(n - w) + 1) for all n -w >= 0```

1. (5 pts.) Show that this problem has the overlapping subproblems property.
    - ![image](./final1Q1.jpg)
1. (15 pts.) Write a recursive algorithm (as a pseudocode) that returns the smallest
   number of items the thief can steal using a bag capacity of n.
    - [click to see solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/main/src/Final_Exam01.java#L8-L20)

1. (5 pts.) Show that the greedy choice of taking the largest weight items into the bag first
   fails to lead to an optimal solution.
    - ![](./final1Q1-d.jpg)
1. (15 pts.) Write a dynamic programming algorithm (as a pseudocode) for finding the
   smallest number of items the thief can steal using a bag capacity of n.
    - [click to see solution](https://gitlab.com/algorithms39/oldexamquestions/-/blob/main/src/Final_Exam01.java#L22-L41)

The another solution is ``minItemsDynamicAnotherSolution()``:
 - ![](thief_dynamic.jpeg)

İlk 5 farklı durum, oluşabilecek sub-problemler. Bu 5 farklı sub problem, her biri için kendisinden 5 sonraki indexte tekrar ettiği için bu değerleri 5 sonrasına +1 olarak kaydettim sürekli.

1. (10 pts.) Provide the running time of your dynamic programming algorithm. Explain.
    - ``` O(n*weights.length) outer loop goes from 0 to n and inner loop goes from 0 to weights.length -1  ```

## Q2

**2.** (20 pts.) Assume that you are creating an array data structure that has a fixed size of _n_.
You want to backup and empty this array after every _n_ insertion operations. Unfortunately,
the backup operation is quite expensive, it takes _n_ time to do the backup. Insertions without
a backup just take _1_ time unit. Show that you can do backups in _O(1)_ amortized time.

1. (10 pts.) Use the accounting method for your proof. Explain in sufficient detail.
   -  ``` lets say 1 insertion costs 1$. n insertins costs n$. when there is a expansion copying the previous elements and adding new ones costs n to be able to prevent this 1 insertion cost 2$. one for insertion and other one is saved for the further. when we need to the copy array we use these savings. so it leads to amortized time O(1) ``` 
1. (10 pts.) Use the potential method for your proof. Explain in sufficient detail.
   - 
## Q3

**3.** (20 pts.) A university has two student clubs. The number of students registered to the first
club is _m_ and their IDs are stored in an array _A_ (with _m_ elements) whereas the number of
students registered to the second club is _n_ and their IDs are stored in an array _B_ (with _n_
elements), where _m≤n_. A student might be registered to either one of these clubs or both.
We want to decide how many students are registered to both clubs.
Given two arrays _A_ and _B_ along with their lenghts _m_ and _n_ , write a _O(mlogn)_ algorithm (as a
pseudocode) to find the number of elements that are registered to both clubs. For example,
when _A_ is _[2, 6, 3, 9, 11, 8]_ and _B_ is _[3, 11, 7, 4, 2, 5, 1]_ , the algorithm must return _3_
corresponding to the students with IDs _2, 3_ and _11_. Inside your pseudocode, you are allowed
to use functions that are already defined in class videos, slides and book. Also, explain why
your running time is _O(mlogn)_ in sufficient detail.
```
   - Sort array A in increasing order using a sorting algorithm with a time complexity of O(mlogm) (such as merge sort).
     Sort array B in increasing order using a sorting algorithm with a time complexity of O(nlogm) (such as merge sort).
     Initialize a counter variable count to 0.
     Initialize two variables i and j to 0, representing the current indices in arrays A and B respectively.
     While i < m and j < n:
     a. If A[i] < B[j], increment i.
     b. If A[i] > B[j], increment j.
     c. If A[i] == B[j], increment count and increment both i and j.
```



